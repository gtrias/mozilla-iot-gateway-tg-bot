defmodule App.Clients.ApiGatew do
  def iot_list do
    token = "some_token_from_another_request"
    url = "https://example.com/api/endpoint_that_needs_a_bearer_token"
    headers = ["Authorization": "Bearer #{token}", "Accept": "Application/json; Charset=utf-8"]
    options = [ssl: [{:versions, [:'tlsv1.2']}], recv_timeout: 500]
    {:ok, response} = HTTPoison.get(url, headers, options)
  end
end
